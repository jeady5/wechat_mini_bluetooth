//index.js
//获取应用实例
const app = getApp()
//用于显示全部设备
let all_devices = {}
//用于备份原设备列表
let devices_bak = {}
//当前连接的蓝牙设备id
let current_device_id = ""

Page({
  data: {
    bluetooth_list: {
      'abc': '123',
      'sdf': 123
    },
    scan_bt_bt_value: "扫描蓝牙设备",
    show_none_name: '显示未知设备'
  },
  onLoad: function () {
    wx.setNavigationBarTitle({
      title: '蓝牙测试工具',
    })
  },
  test(){
    //获取蓝牙适配器的状态
    console.log('获取蓝牙适配器的状态')
    wx.getBluetoothAdapterState({
      success: (result) => {
        console.log(result)
      },fail(e){
        console.error(e)
      }
    })
    //获取处于蓝牙连接状态的设备
    // wx.getConnectedBluetoothDevices({
    //   services: [],
    // })
    //蓝牙特征值变化时的notify
    // wx.notifyBLECharacteristicValueChange({
    //   characteristicId: 'characteristicId',
    //   deviceId: 'deviceId',
    //   serviceId: 'serviceId',
    //   state: true,
    // })
  },
  disconnect_device_bt: function(){
    wx.closeBluetoothAdapter({
      success: (res) => {
        console.log(res)
      },
      fail(res){
        console.error(res)
      }
    })
  },
  get_device_service_character: function(){
    wx.getBLEDeviceServices({
      deviceId: current_device_id,
      success(e){
        //获取到当前蓝牙id对应的所有服务
        console.log(e.services)
        e.services.forEach((service)=>{
          console.log(service)
          wx.getBLEDeviceCharacteristics({
            deviceId: current_device_id,
            serviceId: service.uuid,
            success(e){
              //获取到当前设备指定服务对应的特征值
              console.log(e.characteristics)
            },
            fail(e){
              console.error(e)
            }
          })
        })
      },fail(e){
        console.error(e)
      }
    })
  },
  //扫描/停止扫描蓝牙设备
  scan_blutooth: function () {
    let that = this
    if (["扫描蓝牙设备", "重新扫描"].indexOf(this.data.scan_bt_bt_value) != -1) {
      this.setData({
        scan_bt_bt_value: "停止扫描",
        bluetooth_list: {}
      })
      wx.showToast({
        title: '开始扫描',
        icon: 'none'
      })
      //关闭当前蓝牙适配器
      wx.closeBluetoothAdapter({
        success: (res) => {},
      })
      //打开蓝牙适配器
      wx.openBluetoothAdapter({
        success(e) {
          console.log("蓝牙适配器打开成功")
          //开始搜索蓝牙设备，每秒报备一次
          wx.startBluetoothDevicesDiscovery({
            interval: 1000,
            allowDuplicatesKey: false,
            success(e) {
              console.log('start discovery')
            },
            fail(e) {
              console.error(e)
            }
          })
        },
        fail(e) {
          console.error(e)
        }
      })
      //每秒报备时调用此api
      wx.onBluetoothDeviceFound((result) => {
        result.devices.forEach(device => {
          if (device.name != '') {
            that.data.bluetooth_list[device.deviceId] = device.name
            devices_bak[device.deviceId] = device.name
          } else {
            if (all_devices.length == [])
              that.data.bluetooth_list[device.deviceId] = device.deviceId
          }
          this.setData({
            bluetooth_list: that.data.bluetooth_list
          })
        });
      })
      //当蓝牙状态改变时执行
      wx.onBLEConnectionStateChange((result) => {
        console.log("蓝牙状态改变", result)
        if (result.connected) {
          wx.showToast({
            title: '蓝牙已连接',
          })
        } else {
          wx.showToast({
            title: '蓝牙未连接',
            icon: 'none'
          })
        }
      })
    } else {
      this.setData({
        scan_bt_bt_value: "重新扫描",
      })
      //停止蓝牙扫描
      wx.stopBluetoothDevicesDiscovery({
        success: (res) => {
          wx.showToast({
            title: '停止扫描',
            duration: 1000,
            icon: 'none'
          })
        }
      })
    }
  },
  //连接设备
  bluetooth_connect: function (e) {
      // //关闭蓝牙连接
      // wx.closeBLEConnection({
      //   deviceId: current_device_id,
      //   success(e){
      //     console.log(e)
      //   },
      //   fail(e){
      //     console.error(e)
      //   }
      // })
      // //取消监听蓝牙状态改变
      // wx.offBLEConnectionStateChange(function(){})
      // //关闭当前蓝牙适配器
      // wx.closeBluetoothAdapter({
      //   success: (res) => {},
      // })
    wx.showLoading({
      title: '连接中',
      mask: true
    })
    //当连接蓝牙时停止搜索
    wx.stopBluetoothDevicesDiscovery({
      success: (res) => {},
    })
    //获取所要连接蓝牙的设备id
    current_device_id = e.target.id
    //创建连接
    wx.createBLEConnection({
      deviceId: current_device_id,
      success(e) {
        console.log(e)
        wx.hideLoading({
          success: (res) => {},
        })
        wx.stopBluetoothDevicesDiscovery({
          success: (res) => {},
        })
      },
      fail(e) {
        wx.hideLoading({
          success: (res) => {},
        })
        console.error(e)
        switch (e.errCode) {
          case 0:
            wx.showToast({
              title: '连接成功',
            })
            break
          case -1:
            wx.showToast({
              title: '设备已连接',
            })
            break
          case 10003:
            wx.showToast({
              title: '连接失败',
              icon: 'none'
            })
            break
          case 10012:
            wx.showToast({
              title: '连接超时',
              icon: 'none'
            })
            break
          default:
            wx.showToast({
              title: e.errMsg,
              icon: 'none'
            })
        }
      }
    })
  },
  //是否显示没有命名的蓝牙
  bt_filter_switch: function () {
    if (this.data.show_none_name == "显示未知设备") {
      this.setData({
        show_none_name: "隐藏位置设备"
      })
      wx.getBluetoothDevices({
        success: (result) => {
          result.devices.forEach(device => {
            all_devices[device.deviceId] = device.name
          });
          this.setData({
            bluetooth_list: all_devices
          })
        },
      })
    } else {
      this.setData({
        show_none_name: "显示未知设备"
      })
      all_devices = {}
      this.setData({
        bluetooth_list: devices_bak
      })
    }
  }
})